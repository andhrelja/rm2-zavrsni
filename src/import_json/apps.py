from django.apps import AppConfig


class ImportJsonConfig(AppConfig):
    name = 'import_json'
