from django.shortcuts import render, redirect, reverse
from django.conf import settings
from django.contrib import messages
from django.views import generic
from . import utils, validate
from .models import MoodleKolegij, MoodleKategorija, MoodleZadatak
from .forms import ImportJsonForm, MoodleKolegijForm, MoodleKategorijaForm, MoodleZadatakForm, MoodleOdgovorForm

from datetime import date
import json
import os


def import_json(request):
    if request.method == "POST":
        form = ImportJsonForm(request.POST, request.FILES)
        if form.is_valid():
            json_file = request.FILES['json_file']

            try:
                dictionary = validate.validate_uploaded_file(json_file)
            except (ValueError, TypeError, KeyError) as e:
                messages.warning(request, e, extra_tags="bg-danger")
            else:
                dictionary = utils.transform_dictionary(dictionary)
                utils.write_dictionary(dictionary, json_file.name)

                messages.success(request, "Pitanja uspješno uvezena", extra_tags="bg-success")
                return redirect(reverse("import_json:list-form", kwargs={"fn": json_file.name}))
    else:
        form = ImportJsonForm()

    context = {
        'form': form
    }

    return render(request, "import_json/import_json_form.html", context)


def list_form(request, fn):
    today = date.today()
    today = date.strftime(today, "%Y-%m-%d")
    file_path = os.path.join(settings.MEDIA_ROOT, today, fn)

    with open(file_path, "r", encoding='utf-8') as f:
        json_string = f.read()
        dictionary = json.loads(json_string)

    kategorija_form = MoodleKategorijaForm(dictionary['kolegij'])
    kolegij_form = MoodleKolegijForm(dictionary['kolegij'])

    zadaci_forms = list()
    for zadatak in dictionary['zadaci']:
        zadatak_dict = dict()
        zadatak_dict['zadatak'] = MoodleZadatakForm(zadatak)

        rjesenje = {
            'name': zadatak['rjesenje'],
            'correct': 1,
            'feedback': "",
            'fraction_correct': zadatak['fraction_correct'],
            'moodle_zadatak_id': zadatak['idnumber']
        }
        zadatak_dict['odgovori'] = [MoodleOdgovorForm(rjesenje)]

        for odgovor in zadatak['odgovori']:
            odgovor = {
                'name': odgovor,
                'correct': 0,
                'feedback': "",
                'fraction_correct': zadatak['fraction_incorrect'],
                'moodle_zadatak_id': zadatak['idnumber']
            }
            zadatak_dict['odgovori'].append(MoodleOdgovorForm(odgovor))
        zadaci_forms.append(zadatak_dict)

    context = {
        'kategorija_form': kategorija_form,
        'kolegij_form': kolegij_form,
        'zadaci_forms': zadaci_forms
    }

    return render(request, "import_json/list_form.html", context)


def save_questions(request):
    if request.method == "POST":
        course_name = request.POST.get('course_name', None)
        category_name = request.POST.get('category_name', None)

        kategorija_form = MoodleKategorijaForm({'category_name': category_name})
        kolegij_form = MoodleKolegijForm({'course_name': course_name})

        if kolegij_form.is_valid():
            moodle_kolegij = kolegij_form.save()
        else:
            moodle_kolegij = MoodleKolegij.objects.get(course_name=course_name)

        if kategorija_form.is_valid():
            moodle_kategorija = kategorija_form.save(commit=False)
            moodle_kategorija.moodle_kolegij = moodle_kolegij
            moodle_kategorija.save()
        else:
            moodle_kategorija = MoodleKategorija.objects.get(category_name=category_name)

        i, j = 0, 0
        postojeci_zadaci = list()
        zadaci, odgovori = utils.parse_post(request.POST)
        for zadatak in zadaci:
            zadatak_form = MoodleZadatakForm(zadatak)

            if zadatak_form.is_valid():
                zadatak = zadatak_form.save(commit=False)
                zadatak.moodle_kategorija = moodle_kategorija
                zadatak.save()

                for odgovor in odgovori[zadatak.idnumber]:
                    odgovor_form = MoodleOdgovorForm(odgovor)
                    if odgovor_form.is_valid():
                        odg = odgovor_form.save(commit=False)
                        odg.moodle_zadatak = zadatak
                        odg.save()

                i += 1
            else:
                j += 1
                postojeci_zadaci.append(MoodleZadatak.objects.get(name=zadatak['name']))
        
        if i > 0:
            messages.success(request, "{0} zadataka uspješno spremljeno".format(i), extra_tags="bg-success")
        if j > 0:
            messages.warning(request, "{0} zadataka već postoji: {1}".format(j, postojeci_zadaci), extra_tags="bg-warning")    
        
        return redirect("home:index")


class MoodleZadatakDetailView(generic.DetailView):
    model = MoodleZadatak
