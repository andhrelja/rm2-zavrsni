from . import models as choices
import json

VALID_TYPES = {
    'kolegij': {
        'course_name' : str,
        'category'    : str
    },
    'zadaci': {
        'naziv'     : str,
        'tekst'     : str,  
        'rjesenje'  : str,
        'odgovori'  : list,
        'type'      : str,
        'single'    : bool,
        'penalty'   : float,
        'idnumber'  : str,
        'defaultgrade'      : float,
        'fraction_correct'  : float,
        'fraction_incorrect': float,
        'generalfeedback'   : str,
        'shuffleanswers'    : bool,
        'answernumbering'   : str,
        'correctfeedback'   : str,
        'incorrectfeedback' : str,
        'partiallycorrectfeedback': str
    }
}

VALID_DATA = {
    'zadaci': {
        'type'      : [tup[1] for tup in choices.TYPE_CHOICES],
        'single'    : [tup[0] for tup in choices.SINGLE_CHOICES],
        'penalty'   : [float(tup[1]) for tup in choices.PENALTY_CHOICES],
        'fraction_correct'  : [float(tup[1]) if tup[1] != 'None' else 0 for tup in choices.FRACTION_CHOICES],
        'fraction_incorrect': [float(tup[1]) if tup[1] != 'None' else 0 for tup in choices.FRACTION_CHOICES],
        'answernumbering'   : [tup[0] for tup in choices.ANSWERNUMBERING_CHOICES]
    }
}

def validate_uploaded_file(content):
    json_string = str()

    if content.multiple_chunks():
        for chunk in content.chunks():
            json_string += chunk.decode('utf8')
    else:
        json_string = content.read()
    
    dictionary = json.loads(json_string)
    validate_dictionary(dictionary)
    return dictionary


def validate_dictionary(dictionary):
    keys = ["kolegij", "zadaci"]

    for key in keys:
        try:
            item = dictionary[key]
        except KeyError:
            raise KeyError("Ključ '{0}' ne postoji u datoteci".format(key))
        except TypeError:
            raise TypeError("Dobiven neočekivan tip podatka za ključ \"{0}\". Očekuje se '{1}'".format(key, type(VALID_TYPES[key])))
        else:
            if key != 'zadaci' and type(item) is dict:
                validate_dictionary_types(item, key)
            elif key == 'zadaci' and type(item) is list:
                validate_zadaci(item)
                validate_zadaci_data(item)


def validate_dictionary_types(dictionary, dictionary_key):
    for key, item in dictionary.items():
        if key not in VALID_TYPES[dictionary_key]:
            raise KeyError("Nedopušteni ključ {0}".format(key))
        else:
            if VALID_TYPES[dictionary_key][key] is float:
                try:
                    float(item)
                except ValueError:
                    raise ValueError("Nedopuštena vrijednost podatka za '{0}' - vrijednost mora biti tipa {2}".format(key, key, VALID_TYPES[dictionary_key][key]))
            elif type(item) is not VALID_TYPES[dictionary_key][key]:
                raise TypeError("Nedopušten tip podatka {0} za '{1}' - vrijednost mora biti tipa {2}".format(type(item), key, VALID_TYPES[dictionary_key][key]))


def validate_zadaci(zadaci):
    for zadatak in zadaci:
        validate_dictionary_types(zadatak, 'zadaci')


def validate_zadaci_data(zadaci):
    for zadatak in zadaci:
        for key, item in VALID_DATA['zadaci'].items():
            if zadatak[key] not in item:
                raise ValueError("Nepoznati podatak \"{0}\" za ključ '{1}'. Mora biti jedan od {2}".format(zadatak[key], key, item))
