from django.urls import path
from . import views

app_name = "import_json"

urlpatterns = [
    path('', views.import_json, name="json"),
    path('list/save/', views.save_questions, name="save-questions"),
    path('list/<str:fn>/', views.list_form, name="list-form"),
    path('detail/<int:pk>', views.MoodleZadatakDetailView.as_view(), name="zadatak-detail")
]
