from django.contrib import admin
from .models import MoodleKategorija, MoodleKolegij, MoodleZadatak, MoodleOdgovor

# Register your models here.

admin.site.register(MoodleKategorija)
admin.site.register(MoodleKolegij)
admin.site.register(MoodleZadatak)
admin.site.register(MoodleOdgovor)