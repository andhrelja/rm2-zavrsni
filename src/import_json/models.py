from django.db import models
from django.urls import reverse


COURSE_CHOICES = (
    ('RM1', 'Računalne Mreže 1'),
    ('RM2', 'Računalne Mreže 2'),
)

CATEGORY_CHOICES = (
    (1, '1. kolokvij'),
    (2, '2. kolokvij'),
)

ANSWERNUMBERING_CHOICES = (
    ('abc', 'a., b., c., ...'),
    ('ABC', 'A., B., C., ...'),
    ('123', '1., 2., 3., ...'),
    ('iii', 'i., ii., iii., ...'),
    ('III', 'I., II., III., ...'),
)

FRACTION_CHOICES = (
    (0, 'None'),
    (1, '100'),
    (2, '90'),
    (3, '83.33333'),
    (4, '80'),
    (5, '75'),
    (6, '70'),
    (7, '66.66667'),
    (8, '60'),
    (9, '50'),
    (10, '40'),
    (11, '33.33333'),
    (12, '30'),
    (13, '25'),
    (14, '20'),
    (15, '16.66667'),
    (16, '14.28571'),
    (17, '12.5'),
    (18, '11.11111'),
    (19, '10'),
    (20, '5'),
    (21, '-100'),
    (22, '-90'),
    (23, '-83.33333'),
    (24, '-80'),
    (25, '-75'),
    (26, '-70'),
    (27, '-66.66667'),
    (28, '-60'),
    (20, '-50'),
    (30, '-40'),
    (31, '-33.33333'),
    (32, '-30'),
    (33, '-25'),
    (34, '-20'),
    (35, '-16.66667'),
    (36, '-14.28571'),
    (37, '-12.5'),
    (38, '-11.11111'),
    (39, '-10'),
    (40, '-5'),
)

PENALTY_CHOICES = (
    (0, '0'),
    (1, '100'),
    (2, '50'),
    (3, '33.33333'),
    (4, '25'),
    (5, '20'),
    (5, '10'),
)

TYPE_CHOICES = (
    (0, 'multichoice'),
    (1, 'truefalse'),
    (2, 'shortanswer'),
    (3, 'matching'),
    (4, 'cloze'),
    (5, 'essay'),
    (6, 'numerical'),
    (7, 'description')
)

SINGLE_CHOICES = (
    (0, 'Više točnih odgovora'),
    (1, 'Jedan točan odgovor'),
)


class MoodleZadatak(models.Model):

    name                = models.CharField("Naziv pitanja", max_length=255)
    questiontext        = models.TextField("Tekst pitanja")
    defaultgrade        = models.FloatField("Ocjena za pitanje")
    generalfeedback     = models.TextField("Napomena", null=True, blank=True, help_text="Napomena o pitanju namijenjena studentu nakon prikaza ocjene")
    idnumber            = models.CharField("Moodle ID", null=True, blank=True, help_text="Moodleova oznaka za ID pitanja", max_length=50)
    single              = models.BooleanField("Jedan ili više točnih odgovora", choices=SINGLE_CHOICES, default=1)
    shuffleanswers      = models.BooleanField("Mješanje poretka odgovora", default=1)
    answernumbering     = models.CharField("Oznake odgovora", default=ANSWERNUMBERING_CHOICES[0][0], choices=ANSWERNUMBERING_CHOICES, help_text="Oznaka za ispis odgovora u sustavu Moodle", max_length=3)
    penalty             = models.FloatField("Penal za netočni pokušaj", default=PENALTY_CHOICES[0][0], choices=PENALTY_CHOICES)

    type                = models.IntegerField("Tip Moodle zadataka", default=TYPE_CHOICES[0][0], choices=TYPE_CHOICES)
    moodle_kategorija   = models.ForeignKey("import_json.MoodleKategorija", verbose_name="moodle_kategorija", on_delete=models.CASCADE)

    correctfeedback     = models.CharField("Napomena - Točan odgovor", null=True, blank=True, max_length=50)
    incorrectfeedback   = models.CharField("Napomena - Netočan odgovor", null=True, blank=True, max_length=50)   
    partiallycorrectfeedback = models.CharField("Napomena - Nepotpun odgovor", null=True, blank=True, max_length=50)


    class Meta:
        verbose_name = "Moodle Zadatak"
        verbose_name_plural = "Moodle Zadatci"

    def __str__(self):
        return "<MoodleZadatak: name={0} grade={1}>".format(self.name, self.defaultgrade)

    def get_absolute_url(self):
        return reverse("import_json:zadatak-detail", kwargs={"pk": self.pk})


class MoodleOdgovor(models.Model):

    name    = models.CharField("Odgovor", max_length=255)
    correct = models.BooleanField("Točan odgovor", default=False)

    feedback            = models.TextField("Napomena", null=True, blank=True, help_text="Napomena o individualnom odgovoru namijenjena studentu nakon prikaza ocjene")
    fraction_correct    = models.FloatField("Udio točnog odgovora u bodovanju", choices=FRACTION_CHOICES)

    moodle_zadatak      = models.ForeignKey("import_json.MoodleZadatak", verbose_name="moodle_zadatak", on_delete=models.CASCADE)

    @property
    def get_fraction_correct(self):
        if self.fraction_correct == 0:
            return 0
        else:
            return self.get_fraction_correct_display()


    class Meta:
        verbose_name = "Moodle Odgovor"
        verbose_name_plural = "Moodle Odgovori"

    def __str__(self):
        return self.name



class MoodleKolegij(models.Model):

    course_name = models.CharField("Naziv kolegija", choices=COURSE_CHOICES, help_text="Naziv kolegija u sustavu Moodle", max_length=3)

    class Meta:
        verbose_name = "Moodle Kolegij"
        verbose_name_plural = "Moodle Kolegiji"

    def __str__(self):
        return self.course_name

    def get_absolute_url(self):
        return reverse("import_json:kolegij-detail", kwargs={"pk": self.pk})


class MoodleKategorija(models.Model):

    category_name  = models.CharField("Naziv kategorije", max_length=64)
    moodle_kolegij = models.ForeignKey("import_json.MoodleKolegij", verbose_name="moodle_kolegij", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Moodle Kategorija"
        verbose_name_plural = "Moodle Kategorije"

    def __str__(self):
        return "{0} - {1}".format(self.moodle_kolegij, self.category_name)


