from django.conf import settings
from . import models as choices
from .models import MoodleZadatak
from querystring_parser import parser

from datetime import date
import json
import os

get_choice = lambda choice, value: [tup[0] for tup in choice if tup[1] == value][0]

def transform_dictionary(dictionary):
    dictionary['kolegij']['category_name'] = dictionary['kolegij'].pop('category')
    for i, zadatak in enumerate(dictionary['zadaci']):
        dictionary['zadaci'][i]['name'] = dictionary['zadaci'][i].pop('naziv')
        dictionary['zadaci'][i]['questiontext'] = dictionary['zadaci'][i].pop('tekst')
        dictionary['zadaci'][i]['type'] = get_choice(choices.TYPE_CHOICES, zadatak['type'])
        dictionary['zadaci'][i]['single'] = int(zadatak['single'])
        dictionary['zadaci'][i]['shuffleanswers'] = int(zadatak['shuffleanswers'])
        dictionary['zadaci'][i]['fraction_correct'] = get_choice(choices.FRACTION_CHOICES, str(zadatak['fraction_correct']))
        dictionary['zadaci'][i]['fraction_incorrect'] = get_choice(choices.FRACTION_CHOICES, str(zadatak['fraction_incorrect']).replace("0", "None"))
    return assign_ids(dictionary)

def assign_ids(dictionary):
    zadatak = MoodleZadatak.objects.last()

    try:
        min_id = zadatak.id
    except AttributeError:
        min_id = 0
    
    for i, zadatak in enumerate(dictionary['zadaci']):
        dictionary['zadaci'][i]['idnumber'] = str(min_id + i + 1)
    return dictionary

def write_dictionary(dictionary, file_name):
    today = date.today()
    today = date.strftime(today, "%Y-%m-%d")
    directory = os.path.join(settings.MEDIA_ROOT, today)

    if not os.path.isdir(directory):
        os.mkdir(directory)

    file_path = os.path.join(directory, file_name)
    with open(file_path, "w", encoding='utf-8') as f:
        json.dump(dictionary, f, ensure_ascii=False)
    
    return file_path


def parse_post(post):
    zadaci = dict()
    post_dict = parser.parse(post.urlencode())

    for key, item in post_dict.items():
        if type(item) is dict:
            zadaci[key] = [value for _, value in item.items()]

    odgovori = {
        'name': zadaci.pop('odgovor_name'),
        'correct': zadaci.pop('odgovor_correct'),
        'feedback': zadaci.pop('odgovor_feedback'),
        'fraction_correct': zadaci.pop('odgovor_fraction_correct'),
        'moodle_zadatak_id': zadaci.pop('odgovor_moodle_zadatak_id')
    }

    return transform_zadaci(zadaci), transform_odgovori(odgovori)

def transform_zadaci(zadaci):
    zadaci_list = list()
    for key, items in zadaci.items():
        for i, pitanje in enumerate(items):
            try:
                zadaci_list[i].update({ key: pitanje })
            except IndexError:
                zadaci_list.append({ key: pitanje })
    return zadaci_list

def transform_odgovori(odgovori):
    odgovori_list = list()
    for key, items in odgovori.items():
        for i, item in enumerate(items):
            try:
                odgovori_list[i]
            except IndexError:
                odgovori_list.append([])
            
            for j, odgovor in item.items():
                try:
                    odgovori_list[i][j].update({ key: odgovor })
                except IndexError:
                    odgovori_list[i].append({ key: odgovor })
    
    return transform_odgovori_list(odgovori_list)


def transform_odgovori_list(odgovori_list):
    odgovori_dict = dict()
    for odgovori in odgovori_list:
        zadatak_id, odgovori = get_zadatak_id(odgovori)
        odgovori_dict.update({
            zadatak_id: odgovori
        })
    return odgovori_dict


def get_zadatak_id(odgovori_list):
    id = odgovori_list[0].pop('moodle_zadatak_id')
    for odgovor in odgovori_list[1:]:
        idnumber = odgovor.pop('moodle_zadatak_id')
        if idnumber != id:
            raise ValueError("ID Odgovora i Pitanja međusobno ne odgovaraju")
    return id, odgovori_list
