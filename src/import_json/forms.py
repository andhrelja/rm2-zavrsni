from django import forms
from django.core.exceptions import ValidationError
from .models import MoodleKolegij, MoodleKategorija, MoodleZadatak, MoodleOdgovor

class ImportJsonForm(forms.Form):
    json_file = forms.FileField(label="JSON datoteka", max_length=255, required=True, widget=forms.FileInput(attrs={'class': 'form-control'}))

class MoodleKolegijForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        course_name = cleaned_data.get('course_name')

        matching_courses = MoodleKolegij.objects.filter(course_name=course_name)
        if self.instance:
            matching_courses = matching_courses.exclude(pk=self.instance.pk)
        if matching_courses.exists():
            msg = "Moodle Kolegij: Kolegij već postoji"
            raise ValidationError(msg)
        else:
            return self.cleaned_data


    class Meta:
        model = MoodleKolegij
        fields = ('course_name',)
        widgets = {
            'course_name': forms.TextInput(attrs={'class': 'form-control'}),
        }

class MoodleKategorijaForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        category_name = cleaned_data.get('category_name')

        matching_categories = MoodleKategorija.objects.filter(category_name=category_name)
        if self.instance:
            matching_categories = matching_categories.exclude(pk=self.instance.pk)
        if matching_categories.exists():
            msg = "Moodle Kategorija: Kategorija već postoji"
            raise ValidationError(msg)
        else:
            return self.cleaned_data


    class Meta:
        model = MoodleKategorija
        fields = ('category_name',)
        widgets = {
            'category_name': forms.TextInput(attrs={'class': 'form-control'}),
        }



class MoodleZadatakForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        name = cleaned_data.get('name')

        matching_zadaci = MoodleZadatak.objects.filter(name=name)
        if self.instance:
            matching_zadaci = matching_zadaci.exclude(pk=self.instance.pk)
        if matching_zadaci.exists():
            msg = "Moodle Zadatak: Zadatak već postoji"
            raise ValidationError(msg)
        else:
            return self.cleaned_data

    
    class Meta:
        model = MoodleZadatak
        fields = (
            'name',
            'questiontext',
            'defaultgrade',
            'idnumber',
            'single',
            'shuffleanswers',
            'answernumbering',
            'generalfeedback',
            'penalty',
            'type',
            'correctfeedback',
            'incorrectfeedback',
            'partiallycorrectfeedback'
        )

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'questiontext': forms.Textarea(attrs={'class': 'form-control'}),
            'defaultgrade': forms.NumberInput(attrs={'class': 'form-control'}),
            'generalfeedback': forms.Textarea(attrs={'class': 'form-control'}),
            'idnumber': forms.TextInput(attrs={'class': 'form-control'}),
            'single': forms.Select(attrs={'class': 'custom-select'}),
            'shuffleanswers': forms.CheckboxInput(attrs={'class': 'form-check'}),
            'answernumbering': forms.Select(attrs={'class': 'custom-select'}),
            'penalty': forms.Select(attrs={'class': 'custom-select'}),
            'type': forms.Select(attrs={'class': 'custom-select'}),
            'correctfeedback': forms.TextInput(attrs={'class': 'form-control'}),
            'incorrectfeedback': forms.TextInput(attrs={'class': 'form-control'}),
            'partiallycorrectfeedback': forms.TextInput(attrs={'class': 'form-control'})
        }


class MoodleOdgovorForm(forms.ModelForm):
    moodle_zadatak_id = forms.IntegerField(label="ID - Moodle Zadatak", required=False, widget=forms.HiddenInput())
    
    def clean(self):
        cleaned_data = super().clean()
        name = cleaned_data.get('name')
        moodle_zadatak_id = cleaned_data.get('moodle_zadatak_id')

        matching_odgovori = MoodleOdgovor.objects.filter(name=name, moodle_zadatak_id=moodle_zadatak_id)
        if self.instance:
            matching_odgovori = matching_odgovori.exclude(pk=self.instance.pk)
        if matching_odgovori.exists():
            msg = "Moodle Odgovor: Odgovor već postoji"
            raise ValidationError(msg)
        else:
            return self.cleaned_data

    class Meta:
        model = MoodleOdgovor
        fields = (
            'name',
            'correct',
            'feedback',
            'fraction_correct',
            'moodle_zadatak_id'
        )
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'correct': forms.CheckboxInput(attrs={'class': 'form-check'}),
            'feedback': forms.Textarea(attrs={'class': 'form-control'}),
            'fraction_correct': forms.Select(attrs={'class': 'custom-select'})
        }
