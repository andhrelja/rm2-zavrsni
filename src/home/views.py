from django.shortcuts import render
from import_json.models import MoodleZadatak


def index(request):
    zadaci = MoodleZadatak.objects.all()
    context = {
        'zadaci': zadaci
    }
    return render(request, "index.html", context)

