from django import forms
from import_json.models import MoodleKategorija


class MoodleKategorijaForm(forms.Form):
    category = forms.ModelChoiceField(label="Kategorija", queryset=MoodleKategorija.objects.all(),
            required=True, widget=forms.Select(attrs={'class': 'custom-select'}))
