import xml.etree.ElementTree as ET
import json


def xml_baza(kolegij='RM1'):
    quiz = ET.Element('quiz')
    
    question = ET.SubElement(quiz, 'question')
    question.set('type', 'category')

    category = ET.SubElement(question, 'category')
    category_text = ET.SubElement(category, 'text')
    category_text.text = "$course$/top/Default for {0}".format(kolegij)

    info = ET.SubElement(question, 'info')
    info.set('format', 'moodle_auto_format')
    info_text = ET.SubElement(info, 'text')
    info_text.text = "The default category for questions shared in context '{0}'.".format(kolegij)

    idnumber = ET.SubElement(question, 'idnumber')

    tree = ET.ElementTree(quiz)
    return tree


def get_xml_string(kategorija, moodle_zadaci, filename):
    tree = xml_baza(kategorija.moodle_kolegij)
    root = tree.getroot()
    
    category_question = ET.SubElement(root, 'question')
    category_question.set('type', 'category')

    category = ET.SubElement(category_question, 'category')
    category_text = ET.SubElement(category, 'text')
    category_text.text = "$course$/top/Default for {0}/{1}".format(kategorija.moodle_kolegij.course_name, kategorija.category_name)

    info = ET.SubElement(category_question, 'info')
    info.set('format', 'html')
    info_text = ET.SubElement(info, 'text')
    info_text.text = "<![CDATA[<p>{0}</p>]]>".format(kategorija.category_name)
    
    idnumber = ET.SubElement(category_question, 'idnumber')

    for zadatak in moodle_zadaci:
        question = ET.SubElement(root, 'question')
        question.set('type', zadatak.get_type_display())

        name = ET.SubElement(question, 'name')
        name_text = ET.SubElement(name, 'text')
        name_text.text = zadatak.name

        questiontext = ET.SubElement(question, 'questiontext')
        questiontext.set('format', 'html')

        questiontext_text = ET.SubElement(questiontext, 'text')
        zadatak_tekst = zadatak.questiontext.replace('\n', '<br>')
        questiontext_text.text = "<![CDATA[<p>{0}</p>]]>".format(zadatak_tekst)

        generalfeedback = ET.SubElement(question, 'generalfeedback')
        generalfeedback.set('format', 'html')

        generalfeedback_text = ET.SubElement(generalfeedback, 'text')
        generalfeedback_text.text = zadatak.generalfeedback.replace('\n', '<br>')

        defaultgrade = ET.SubElement(question, 'defaultgrade')
        defaultgrade.text = str(zadatak.defaultgrade)

        penalty = ET.SubElement(question, 'penalty')
        penalty.text = str(zadatak.penalty)

        hidden = ET.SubElement(question, 'hidden')
        hidden.text = "0"
        
        idnumber = ET.SubElement(question, 'idnumber')
        idnumber.text = zadatak.idnumber

        single = ET.SubElement(question, 'single')
        single_text = bool(zadatak.single)
        single.text = str(single_text).lower()

        shuffleanswers = ET.SubElement(question, 'shuffleanswers')
        shuffleanswers_text = bool(zadatak.shuffleanswers)
        shuffleanswers.text = str(shuffleanswers_text).lower()

        answernumbering = ET.SubElement(question, 'answernumbering')
        answernumbering.text = zadatak.answernumbering

        correctfeedback = ET.SubElement(question, 'correctfeedback')
        correctfeedback.set('format', 'html')
        correctfeedback_text = ET.SubElement(correctfeedback, 'text')
        correctfeedback_text.text = zadatak.correctfeedback

        partiallycorrectfeedback = ET.SubElement(question, 'correctfeedback')
        partiallycorrectfeedback.set('format', 'html')
        partiallycorrectfeedback_text = ET.SubElement(partiallycorrectfeedback, 'text')
        partiallycorrectfeedback_text.text = zadatak.partiallycorrectfeedback

        incorrectfeedback = ET.SubElement(question, 'incorrectfeedback')
        incorrectfeedback.set('format', 'html')
        incorrectfeedback_text = ET.SubElement(incorrectfeedback, 'text')
        incorrectfeedback_text.text = zadatak.incorrectfeedback

        shownumcorrect = ET.SubElement(question, 'shownumcorrect')

        rjesenje = zadatak.moodleodgovor_set.get(correct=True)

        correct_answer = ET.SubElement(question, 'answer')
        fraction_correct = rjesenje.get_fraction_correct_display()
        correct_answer.set('fraction', fraction_correct)
        correct_answer.set('format', 'html')

        correct_answer_text = ET.SubElement(correct_answer, 'text')        
        correct_answer_text.text = "<![CDATA[<p>{0}</p>]]>".format(rjesenje.name)

        feedback = ET.SubElement(correct_answer, 'feedback')
        feedback.set('format', 'html')
        feedback_text = ET.SubElement(feedback, 'text')
        feedback_text.text = "<![CDATA[<p>{0}</p>]]>".format(rjesenje.feedback)

        for odgovor in zadatak.moodleodgovor_set.filter(correct=False):
            answer = ET.SubElement(question, 'answer')
            fraction_correct = odgovor.get_fraction_correct_display()
            answer.set('fraction', fraction_correct)
            answer.set('format', 'html')

            answer_text = ET.SubElement(answer, 'text')
            answer_text.text = "<![CDATA[<p>{0}</p>]]>".format(odgovor)

            feedback = ET.SubElement(answer, 'feedback')
            feedback.set('format', 'html')
            feedback_text = ET.SubElement(feedback, 'text') # Requires data
        
    tree.write(filename, encoding="UTF-8", xml_declaration=True)
    with open(filename, "r", encoding='utf-8') as f:
        file_string = f.read()
    
    file_string = file_string.replace('&lt;', '<')
    file_string = file_string.replace('&gt;', '>')
    return file_string
