from django.urls import path
from . import views

app_name = "export"

urlpatterns = [
    path('', views.category_form, name="category-form"),
    path('list/<int:category_pk>', views.list_zadaci, name="list"),
]
