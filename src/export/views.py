from django.shortcuts import render, redirect, reverse
from django.conf import settings
from django.http import HttpResponse
from django.forms.models import model_to_dict
from django.contrib import messages
from import_json.models import MoodleKolegij, MoodleZadatak, MoodleKategorija
from .forms import MoodleKategorijaForm
from . import utils
import json
import os


def category_form(request):
    form = MoodleKategorijaForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            moodle_kategorija = form.cleaned_data.get('category', None)
            return redirect(reverse("export:list", kwargs={'category_pk': moodle_kategorija.id}))
    
    context = {
        'form': form
    }
    return render(request, "export/category_form.html", context)


def list_zadaci(request, category_pk):
    zadaci = MoodleZadatak.objects.filter(moodle_kategorija_id=category_pk)
    if request.method == "POST":
        moodle_zadatak_ids = request.POST.getlist('moodle_zadatak_ids[]', None)
        zadaci = MoodleZadatak.objects.filter(id__in=moodle_zadatak_ids)

        if 'export-xml' in request.POST:
            return export_xml(request, category_pk, zadaci)
        elif 'export-json' in request.POST:
            return export_json(request, category_pk, zadaci)

    context = {
        'zadaci': zadaci
    }
    return render(request, "export/list.html", context)


def export_json(request, category_pk, zadaci):
    kategorija = MoodleKategorija.objects.get(id=category_pk)
    kolegij = kategorija.moodle_kolegij

    dictionary = {
        'kolegij': {
            'course_name': kolegij.course_name,
            'category': kategorija.category_name
        },
        'zadaci': list()
    }

    for zadatak in zadaci:
        zadatak_dict = model_to_dict(zadatak, fields=(
            'name',
            'questiontext',
            'defaultgrade',
            'idnumber',
            'single',
            'shuffleanswers',
            'answernumbering',
            'generalfeedback',
            'penalty',
            'type',
            'correctfeedback',
            'incorrectfeedback',
            'partiallycorrectfeedback'
        ))
        
        zadatak_dict['odgovori'] = list()
        for odgovor in zadatak.moodleodgovor_set.all():
            odgovor_dict = model_to_dict(odgovor, fields=(
                'name',
                'correct',
                'feedback',
                'fraction_correct',
                'moodle_zadatak_id'
            ))
            zadatak_dict['odgovori'].append(odgovor_dict)

        dictionary['zadaci'].append(zadatak_dict)

    json_string = json.dumps(dictionary, ensure_ascii=False)
    response = HttpResponse(json_string, content_type="text/json")
    response['Content-Disposition'] = 'attachement; filename="{0}.json"'.format(kategorija)
    return response

def export_xml(request, category_pk, zadaci):
    kategorija = MoodleKategorija.objects.get(id=category_pk)
    filename = os.path.join(settings.MEDIA_ROOT, "{0}.xml".format(kategorija))
    xml_string = utils.get_xml_string(kategorija, zadaci, filename)

    response = HttpResponse(xml_string, content_type="text/xml")
    response['Content-Disposition'] = 'attachement; filename="{0}.xml"'.format(kategorija)
    messages.success(request, "Moodle XML uspješno eksportan", extra_tags="bg-success")
    return response